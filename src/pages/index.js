import React from "react"
import { Link } from "gatsby"
import {
  Button,
  Form,
  FormGroup,
  FormLabel,
  FormControl,
  Row,
  Col,
  Image,
} from "react-bootstrap"

import "bootstrap/dist/css/bootstrap.min.css"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div>
      <h1>Bienvenue sur la plateforme d'inscription</h1>
    </div>

    <div className="liens">
      <Link to="/connexion">
        {" "}
        <Button variant="warning"> Connexion</Button>
      </Link>
    </div>

    <div>
      <Form>
        <Form.Group as={Row} controlId="formHorizontalNom">
          <Form.Label column sm={2}>
            Nom
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="name" placeholder="Entrez votre nom" />
          </Col>
        </Form.Group>


        <Form.Group as={Row} controlId="formHorizontalPrenom">
          <Form.Label column sm={2}>
            Prénom
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="name" placeholder="Entrez votre prénom" />
          </Col>
        </Form.Group>  

        <Form.Group as={Row} controlId="formHorizontalPassword">
          <Form.Label column sm={2}>
            Mot de passe
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="password" placeholder="Entrez votre mot de passe" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formHorizontalConfirmer">
          <Form.Label column sm={2}>
            Confirmer
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="password" placeholder="Tapez à nouveau votre mot de passe" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formHorizontalContact">
          <Form.Label column sm={2}>
            Contact
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="name" placeholder="Entrez votre contact" />
          </Col>
        </Form.Group>  

       
          <Form.Group as={Row}>
            <Form.Label as="legend" column sm={2}>
              Service
            </Form.Label>
            <Col sm={10}>
              <Form.Check
                type="radio"
                label="Informatique"
                name="formHorizontalRadios"
                id="formHorizontalRadios1"
              />
              <Form.Check
                type="radio"
                label="Marketing"
                name="formHorizontalRadios"
                id="formHorizontalRadios2"
              />
              <Form.Check
                type="radio"
                label="Commercial"
                name="formHorizontalRadios"
                id="formHorizontalRadios3"
              />
            </Col>
          </Form.Group>
        
        <Form.Group as={Row} controlId="formHorizontalCheck">
          <Col sm={{ span: 6, offset: 5 }}>
            <Form.Check label="Se souvenir de moi" />
          </Col>
        </Form.Group>

        <Form.Group as={Row}>
          <Col sm={{ span: 10, offset: 2 }}>
            <Link to="/connexion">
              {" "}
              <Button type="submit" variant="primary">
                {" "}
                S'inscrire{" "}
              </Button>
            </Link>
          </Col>
        </Form.Group>
      </Form>
    </div>
    <Image src="holder.js/100px250" fluid />
   






  </Layout>
)

export default IndexPage
