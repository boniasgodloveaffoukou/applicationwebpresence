import React from "react"
import { Link } from "gatsby"
import {
  Button,
  Form,
  FormGroup,
  FormLabel,
  FormControl,
  Row,
  Col,
} from "react-bootstrap"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div>
    <h1>Bienvenue sur la plateforme de connexion</h1>      
    </div>


    <div className="liens">
      <Link to="/">
        {" "}
        <Button variant="success"> Inscription</Button>
      </Link>
    
    </div>

      <Form>
        <Form.Group as={Row} controlId="formHorizontalIdentifiant">
          <Form.Label column sm={2}>
            Identifiant
          </Form.Label>
          <Col sm={6}>
            <Form.Control type="name" placeholder="Entrez votre identifiant" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formHorizontalCheck">
          <Col sm={{ span: 10, offset: 2 }}>
            <Form.Check label="Se souvenir de moi" />
          </Col>
        </Form.Group>

        <Form.Group as={Row}>
          <Col sm={{ span: 10, offset: 2 }}>
          <Link to="/liste">
                {" "}
                <Button variant="warning"> Connexion </Button>
              </Link>
          </Col>
        </Form.Group>
      </Form>

  </Layout>
)

export default IndexPage
