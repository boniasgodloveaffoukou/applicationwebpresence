import React from "react"
import { Link } from "gatsby"
import {
  Button,
  Form,
  FormGroup,
  FormLabel,
  FormControl,
  Row,
  Col,
  Table,
} from "react-bootstrap"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div>
    <h1>Bienvenue sur la liste de présence</h1>      
    </div>

    <div className="liens">
      <Link to="/">
        {" "}
        <Button variant="success"> Inscription</Button>
      </Link>
      <Link to="/connexion">
        {" "}
        <Button variant="warning"> Connexion</Button>
      </Link>
      <Link to="/">
                {" "}
      <Button variant="danger "> Retour </Button>
      </Link>
    </div>
    <div>
    <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>Nom</th>
      <th>Prénom</th>
      <th>Identifant</th>
      <th>Présence</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>AFFOUKOU</td>
      <td>Bonias</td>
      <td>@mdo34</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>AMOUSSOU</td>
      <td>Magnolia</td>
      <td>@fat34</td>
      <td></td>
    </tr>

    <tr>
      <td>3</td>
      <td>HOUMENOU</td>
      <td>Mathilde</td>
      <td>@JIt65</td>
      <td></td>
    </tr>

    <tr>
      <td>4</td>
      <td>TISSEGLO</td>
      <td>Lolo</td>
      <td>@guj90</td>
      <td></td>
    </tr>

    <tr>
      <td>5</td>
      <td>DELEKE</td>
      <td>Loic</td>
      <td>@fKL09</td>
      <td></td>
    </tr>

    <tr>
      <td>6</td>
      <td>DOSSAH</td>
      <td>Espoir</td>
      <td>@FSC88</td>
      <td></td>
    </tr>

  </tbody>
</Table>
</div>


  </Layout>
)

export default IndexPage
